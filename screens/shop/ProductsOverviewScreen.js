import React from 'react';
import {FlatList, Button} from 'react-native';
// allows us to tap into redux store and get out our products
// from there
import {useSelector, useDispatch} from 'react-redux';

import ProductItem from '../../components/shop/ProductItem';
import Colours from '../../constants/Colours'
import * as cartActions from '../../store/actions/cart'
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from '../../components/UI/HeaderButton';
const ProductsOverviewScreen=props=>{
    // sueselector takes a function which automatically receives
    // the state (redux state) as an input and which then returns
    // whichever data we wan to use

    // state.products refering to name provided in App.js (rootreducer) 
    const products =useSelector(state=>state.products.availableProducts);
    // we are getting access to useDispatch function
    const dispatch=useDispatch();

    const selectItemHandler=(id, title)=>{
        // props.navigation.navigate('ProductDetail' nurodom i savo key aprasyta navigatione
        props.navigation.navigate('ProductDetail',
        // perduodam parametrus i producdetailsscreen componenta
        {
        productId: id,
        productTitle: title})

    };
    return <FlatList 
    data={products} 
    keyExtractor={item=>item.id}
    // itemData.item yra grynai is flatlisto, imageUrl yra is musu apibrezto modelio productui atvaizduoti
    renderItem={itemData=>
    <ProductItem 
    image={itemData.item.imageUrl}
    title={itemData.item.title}
    price={itemData.item.price}
    onSelect={()=>{
                // // props.navigation.navigate('ProductDetail' nurodom i savo key aprasyta navigatione
                // props.navigation.navigate('ProductDetail',
                // // perduodam parametrus i producdetailsscreen componenta
                // {productId: itemData.item.id,
                // productTitle: itemData.item.title})

                selectItemHandler(itemData.item.id, itemData.item.title)
    }} 
    // onAddToCart={()=>{
    //     // dispatching our action
    //     // cartActions.addToCart() takes our product
    //     dispatch(cartActions.addToCart(itemData.item))

    // }}
    >
{/* is selfclosing componento padarem i ne selfclosing ziuret i ProductItem'a props.childer*/}
        <Button 
            color={Colours.price}
            title="View Details" 
            onPress={()=>{selectItemHandler(itemData.item.id, itemData.item.title)}
 } />
            <Button 
            color={Colours.price}
            title="to Cart" 
            onPress={()=>{dispatch(cartActions.addToCart(itemData.item))}}
            />
    </ProductItem>
}/> ;
}
ProductsOverviewScreen.navigationOptions=navData=>{
    return{
    headerTitle: 'All Products',
    headerLeft: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
    <Item title="Menu" iconName="md-menu" 
    onPress={()=>{
        // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
        navData.navigation.toggleDrawer();
    }}/>
    </HeaderButtons>),
    // adding cart icon
    headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item title="Cart" iconName="md-cart" 
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.navigate('Cart');
        }}/>
    </HeaderButtons>
    )}
};

export default ProductsOverviewScreen;