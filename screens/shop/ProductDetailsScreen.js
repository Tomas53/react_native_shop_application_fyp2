import React from 'react';
import {
    View,
    Button,
    Text,
    ScrollView,
    StyleSheet,
    Image
} from 'react-native';

import {useSelector, useDispatch} from 'react-redux';
import Colours from '../../constants/Colours';
import * as cartActions from '../../store/actions/cart'

const ProductDetailScreen=props=>{
    // const productId = props.navigation.getParam('productId'); yra parametras nurodytas productsOverviewScreen 
    const productId = props.navigation.getParam('productId');
    const selectedProduct = useSelector(state=>
        // state.products yra tai ka mes nurodeme App.js rootReducer
        state.products.availableProducts.find(prod=>prod.id===productId));

    
        const dispatch=useDispatch();
    
        return (
        <ScrollView>
            <Image style={styles.image} source={{uri:selectedProduct.imageUrl}}/>
            <View style={styles.button}>
            <Button color={Colours.primary}title='Add to cart' onPress={()=>{
                dispatch(cartActions.addToCart(selectedProduct))
            }}/>
            </View>
            <Text style={styles.price}>${selectedProduct.price}</Text>
            <Text style={styles.description}>{selectedProduct.description}</Text>
        </ScrollView>
    )

    

};
ProductDetailScreen.navigationOptions=navigationData=>{
    return{

        // getParam("productTitle") gaunam is productOverviewScreen ka nurodem kaip parametra
        headerTitle: navigationData.navigation.getParam('productTitle')
    }
}



const styles=StyleSheet.create({
    image:{
        width: '100%',
        height:300
    },
    actions:{
        marginVertical:10,
        alignItems:'center',
        

    },
    button:{
        width: 100,
        height: 20,
        alignSelf: 'center',
        marginBottom:20

    },
    price:{
        fontSize:20,
        color:'#888',
        textAlign:'center',
        marginVertical:20


    },
    description:{
        fontSize:14,
        textAlign:'center',
        marginHorizontal: 15
    }

});

export default ProductDetailScreen;