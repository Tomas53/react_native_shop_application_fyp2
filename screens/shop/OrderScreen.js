import React from 'react';
import {View, FlatList, Text} from 'react-native';
import { useSelector} from 'react-redux';
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from '../../components/UI/HeaderButton';
import OrderItem from '../../components/shop/OrderItem'
const OrdersScreen=props=>{
    const orders =useSelector(state=>state.orders.orders)//accesing action order

        return (
        <FlatList 
        data={orders} 
        keyExtractor={item=>item.id} 
        renderItem={itemData=><OrderItem amount={
            itemData.item.totalAmount} 
            date={itemData.item.readableDate}
            items={itemData.item.items}//we passing itms into order thus we can output items of our orders
            
            />}/>
        
        )
    };

OrdersScreen.navigationOptions=navData=>{
    return{
        headerTitle: 'Your Orders',
        headerLeft: (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
            <Item title="Menu" iconName="md-menu" 
            onPress={()=>{
                // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
                navData.navigation.toggleDrawer();
            }}/>
            </HeaderButtons>),
    };
    
}
export default OrdersScreen;