import React from 'react';
import {View, Text, FlatList, Button, StyleSheet} from 'react-native'
// using useSelector to get data from our store
import {useSelector, useDispatch} from 'react-redux';

// import React from 'react';
// import { View, Text, FlatList, Button, StyleSheet } from 'react-native';
// import { useSelector } from 'react-redux';
import CartItem from '../../components/shop/CartItem'
import * as cartActions from '../../store/actions/cart'
import * as ordersActions from '../../store/actions/orders'
const CartScreen=props=>{
    const cartTotalAmount = useSelector(state => state.cart.totalAmount)
    // const cartTotalAmount = useSelector(state => state.cart.totalAmount);

    const cartItems=useSelector(state=>{
        const transformedCartItems=[];
        for (const key in state.cart.items){
            transformedCartItems.push({
                productId: key,
                productTitle: state.cart.items[key].productTitle,
                productPrice: state.cart.items[key].productPrice,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum
            });
            
        }//prdedam sortinima pagal id kad orderiai nesikeistu vietomis
        return transformedCartItems.sort((a, b)=> a.productId>b.productId? 1 : -1 );
        
    });

    const dispatch=useDispatch();
return (<View style={styles.screen}>
    <View>
        {/* below we are fixing the -0 problem with Math.round */}
            <Text>Total:{' '}<Text>${Math.round(cartTotalAmount.toFixed(2)*100) / 100}</Text></Text>
            <Button 
            title ="Order Now" 
            disabled={cartItems.length===0}
            onPress={()=>{
                dispatch(ordersActions.addOrder(cartItems, cartTotalAmount))//passing details to action
            }}/>
    </View>
    <FlatList 
    data={cartItems} 
    // keyExtractor lets react know where our key could be found
    // we choose productId as our key
    keyExtractor={data=> data.productId} 
    renderItem={data=>
    <CartItem quantity={data.item.quantity} 
    title={data.item.productTitle} 
    amount={data.item.sum}
    deletable//sets deletable to true thus we can see trach icon on our cart next to orders
    onRemove={()=>{
        dispatch(cartActions.removeFromCart(data.item.productId))
    }}
    />}/>
</View>
)
};


CartScreen.navigationOptions={
    headerTitle: 'Your Cart'
}


const styles=StyleSheet.create({});


export default CartScreen;

// import React from 'react';
// import { View, Text, FlatList, Button, StyleSheet } from 'react-native';
// import { useSelector } from 'react-redux';

// // import Colors from '../../constants/Colors';
// // import CartItem from '../../components/shop/CartItem';

// const CartScreen = props => {
//   const cartTotalAmount = useSelector(state => state.cart.totalAmount);
// //   const cartItems = useSelector(state => {
// //     const transformedCartItems = [];
// //     for (const key in state.cart.items) {
// //       transformedCartItems.push({
// //         productId: key,
// //         productTitle: state.cart.items[key].productTitle,
// //         productPrice: state.cart.items[key].productPrice,
// //         quantity: state.cart.items[key].quantity,
// //         sum: state.cart.items[key].sum
// //       });
// //     }
// //     return transformedCartItems;
// //   });

//   return (
//     <View style={styles.screen}>
//       <View style={styles.summary}>
//         <Text style={styles.summaryText}>
//           Total:{' '}
//           <Text style={styles.amount}>${cartTotalAmount.toFixed(2)}</Text>
//         </Text>
//         <Button
//         //   color={Colors.accent}
//           title="Order Now"
//         //   disabled={cartItems.length === 0}
//         />
//       </View>
//       {/* <FlatList
//         data={cartItems}
//         keyExtractor={item => item.productId}
//         // renderItem={itemData => (
//         // //   <CartItem
//         // //     quantity={itemData.item.quantity}
//         // //     title={itemData.item.productTitle}
//         // //     amount={itemData.item.sum}
//         // //     onRemove={() => {}}
//         // //   />
//         // )}
//       /> */}
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   screen: {
//     margin: 20
//   },
//   summary: {
//     flexDirection: 'row',
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     marginBottom: 20,
//     padding: 10,
//     shadowColor: 'black',
//     shadowOpacity: 0.26,
//     shadowOffset: { width: 0, height: 2 },
//     shadowRadius: 8,
//     elevation: 5,
//     borderRadius: 10,
//     backgroundColor: 'white'
//   },
//   summaryText: {
//     // fontFamily: 'open-sans-bold',
//     fontSize: 18
//   },
//   amount: {
//     // color: Colors.primary
//   }
// });

// export default CartScreen;
