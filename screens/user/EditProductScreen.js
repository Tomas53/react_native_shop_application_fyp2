import React, {useState,  useEffect, useCallback} from 'react';
import {View, Text, StyleSheet, Platform, Alert} from 'react-native';
import { ScrollView, TextInput } from 'react-native-gesture-handler';

import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from '../../components/UI/HeaderButton';


import {useSelector, useDispatch} from 'react-redux'

import * as productActions from '../../store/actions/products'
// import { useEffect } from 'react';
const EditProductScreen=props=>{

    const prodId=props.navigation.getParam('productId');//getting the id of product we want to edit

    // checks if we are in edit mode and thus we populate our screen
    const editedProduct=useSelector(state=>
        state.products.userProducts.find(prod=>prod.id===prodId)//checking if product id you are looking at is the same as product id retrieved from parameters
        );



    const dispatch=useDispatch();//creating dispatch function

    // managing state, if there editedProduct is set basically if we are in edit mode then load in this case title and etc if not then leave empty space   
    const [title, setTitle]=useState(editedProduct ? editedProduct.title : '');
    const [imageUrl, setImageUrl]=useState(editedProduct ? editedProduct.imageUrl : '');
    const [price, setPrice]=useState(editedProduct ? editedProduct.price : '');
    const [description, setDescription]=useState(editedProduct ? editedProduct.description : '');



    




//useCallback ensures the function isnt recreated everytime the component rerenders hus we dont get infiniteloop
    const submitHandler=useCallback(()=>{
        if(editedProduct){//here we checking if we editing or if we adding a product
            dispatch(productActions.updateProduct(prodId,title, description, imageUrl))
           
        }else{
            dispatch(productActions.createProduct(title, description, imageUrl, +price))//+price because price is just a string which does not work
                
        }
    },[dispatch, prodId, title, description, imageUrl, price])//we are writing this empty array [] here thus the function wouoldnt be recreated

    useEffect(()=>{//executes function after a rerender cycle
        props.navigation.setParams({submit: submitHandler});//we are passing an object and adding a key- submit for submitHandler, thus now submit is a parameter that we can retrieve in the header
        //params helps us to connect header and component

    },[submitHandler]);//sumbithandler is our dependency which never changes, and now it only executes one

    return (
        <ScrollView>
            <View style={styles.formControl}>

            <View style={styles.formControl}>
                <Text style={styles.label}>Title</Text>
                <TextInput 
                style={styles.input} 
                value={title} 
                onChangeText={text=>setTitle(text)}/>
            </View>

            <View style={styles.formControl}>
                <Text style={styles.label}>Url image</Text>
                <TextInput style={styles.input}
                value={imageUrl} 
                onChangeText={text=>setImageUrl(text)}/>
            </View>
{/* we are loadind the view below only if we are in adding mode thus if we are in edditing mode we dont see the price input*/}
            {editedProduct ? null :(
            <View style={styles.formControl}>
                <Text style={styles.label}>Price</Text>
                <TextInput style={styles.input}
                value={price} 
                onChangeText={text=>setPrice(text)}/>
            </View>
            )}

            <View style={styles.formControl}>
                <Text style={styles.label}>Description</Text>
                <TextInput style={styles.input}
                value={description} 
                onChangeText={text=>setDescription(text)}/>
            </View>
            </View>

        </ScrollView>
        
    )

    

}

EditProductScreen.navigationOptions=navData=>{//makeing dynamic header
    const submitFn=navData.navigation.getParam('submit');//here we retrieving the submit parameter from props.navigation.setParams({submit: submitHandler});
    return{
        headerTitle: navData.navigation.getParam('productId')//checking if we get productId (it checks wheter we are editing if we get id or adding if we dont get id) 
        ? 'Edit Product' 
        : 'Add Product',
        headerRight:
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item 
        title="Save" iconName={Platform.OS==='android'?"md-checkmark":'ios-checkmark' }
        onPress={submitFn}
        />
        </HeaderButtons>

    }
}


const styles=StyleSheet.create({
    form:{
        margin: 20
    },
    formControl:{
        width:'100%'

    },
    label: {
        marginVertical: 8
    },
    input:{
        paddingHorizontal: 2,
        paddingVertical: 5,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1

    }

})

export default EditProductScreen;