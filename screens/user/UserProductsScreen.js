import React from 'react';
import { FlatList, Button, Alert } from 'react-native';
import {useSelector, useDispatch} from 'react-redux';//to get data from reducer
import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import HeaderButton from '../../components/UI/HeaderButton';
import ProductItem from "../../components/shop/ProductItem"
import Colours from '../../constants/Colours'


import * as productActions from '../../store/actions/products'

const UserProductsScreen=props=>{

    const userProducts=useSelector(state=>state.products.userProducts)//ziuri i root reduceri App.js is ten eina i product reducer folderuje ir accessina userProducts
    const dispatch=useDispatch();

    const editProductHandler=(id)=>{
        props.navigation.navigate('EditProduct', {productId: id})//here we are pointing to the navigation to EditProduct identifier we created(named it in that way)
            //we are forwarding productId in code up
    }


    const deleteHandler=(id)=>{//passing id here
        Alert.alert('Are you sure?', 'do you realy want to delete it',
        [
            {text:'No', style: 'default'},//button nr 1
            {text:'yes', style:'destructive', onPress:()=>{//button nr 2
                    
                    dispatch(productActions.deleteProduct(id));//we are choosing deleteProduct via our actions and then we passing productId
                }
            }
        ])
    }

        return(<FlatList 
        data={userProducts} 
        keyExtractor={item=>item.id}
        renderItem={itemData=><ProductItem image={itemData.item.imageUrl}//itemData.item points to our product model in model folder
        title={itemData.item.title} 
        price={itemData.item.price}
        onSelect={()=>{

            editProductHandler(itemData.item.id)//we are forwarding id to our function

        }} 
      
        >

            <Button 
            color={Colours.price}
            title="Edit" 
            onPress={()=>{
                editProductHandler(itemData.item.id)//we are forwarding id to our function
            }}
  />
            <Button 
            color={Colours.price}
            title="Delete" 
            onPress={()=>{deleteHandler(itemData.item.id)//we forwarding the id from itemData.item.id to deleteHandler
            //     ()=>{
            //     //dispatching our action
            //     dispatch(productActions.deleteProduct(itemData.item.id));//we are choosing deleteProduct via our actions and then we passing productId
            // }
        }}
            />
        </ProductItem>
        
    }
    />)
}

UserProductsScreen.navigationOptions=navData=>{
    return{headerTitle:'Your products',
    headerLeft: (
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item title="Menu" iconName="md-menu" 
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.toggleDrawer();
        }}/>
        </HeaderButtons>
        ),

    headerRight:(
        <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item title="Add" iconName="md-create" 
        onPress={()=>{
            // 'Cart' nameee given in shopnavigtion to tavigato to cart screen
            navData.navigation.navigate('EditProduct');//we are pointing to editproduct however we dont pass any parameters because we are creating a new product
        }}/>
        </HeaderButtons>
    )
}}//pridedam headeri sitam screenui
export default UserProductsScreen;