import moment from 'moment';
class Order {

    constructor(id, items, totalAmount, date){
        this.id=id;//order has standalone id
        this.items=items;
        this.totalAmount=totalAmount;//total items
        this.date=date;//date when order created
    }
    get readableDate(){//kad galetume laika atvaizduot ordriuose
        // return this.date.toLocaleDateString('en-EN',{
        //     year:'numeric',
        //     moth:'long',
        //     day:'numeric',
        //     hour:'2-digit',
        //     minute:'2-digit'

        // })
        return moment(this.date).format('MMMM Do YYYY, hh:mm');//graziai suformatuoja laika androide ir iose
    }
}

export default Order;