import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {createStore, combineReducers} from 'redux';
import {Provider} from 'react-redux';
import productsReducer from './store/reducers/products';
import ShopNavigator from './navigation/ShopNavigator';
import cartReducer from './store/reducers/cart'
import ordersReducer from './store/reducers/orders'


const rootReducer =combineReducers({
  products: productsReducer,
  cart: cartReducer,
  orders: ordersReducer//lets us to dispatch actions
})

const store=createStore(rootReducer);

export default function App() {
  return (
      <Provider store={store}>
        <ShopNavigator/>
      </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
