
import {ADD_TO_CART, REMOVE_FROM_CART} from '../actions/cart'
import CartItem from '../../models/cart-item'
import {ADD_ORDER} from '../actions/orders'
import {DELETE_PRODUCT} from '../actions/products'
// little reducers that are combined in a root reducer that make that big state in the end
// thus this is small sub state
const initialState={
    // array of items
    // items:[],
    // items will be a js objects
    items: {},
    // number of items
    totalAmount: 0

}
// state is initialstate as an default value and
// action is as an argument
export default (state=initialState, action)=>{
    switch(action.type){
        case ADD_TO_CART:
            const addedProduct=action.product;
            const prodPrice=addedProduct.price;
            const prodTitle=addedProduct.title;

            
            let updatedOrNewCartItem;


            // checking if we have already got the item

            if(state.items[addedProduct.id]){
                // aready have item in the cart
                updatedOrNewCartItem=new CartItem(
                    // we get the quantity from the state items for the
                    // id of the added product and we look for quantity, thus
                    // we simply add one to that
                    state.items[addedProduct.id].quantity+1,
                    // cart item for a second argument takes the product price
                    prodPrice,
                    // we are taking the most recent title here from
                    // the product we getting here->  const prodTitle=addedProduct.title;
                    prodTitle,
                    // we are taking the currrent sum plus the product price
                    state.items[addedProduct.id].sum+prodPrice
                );
                // we can return the new state by copying the old state
                // here overwrite the [addedProduct.id] with our updatedCartItem
                // here we actually dont need to copy state(...state) because
                // we are updating its every piece with something(items[] and totalAmount),
                // however if we didint do that we would lose data that we are not updating
                // thus its better to copy state and have all the contents
                // return { ...state, items:{ ...state.items, [addedProduct.id]: updatedCartItem },
                //     totalAmount: state.totalAmount+prodPrice
                // }
            } else{
                // adding a new item
                updatedOrNewCartItem=new CartItem(1,prodPrice, prodTitle, prodPrice);
            }
                // we return a copy of our tate(...state)
                // we set items equal to a new object where we copy our existing state
                // items in(items:{ ...state.items,) and then we add new key([addedProduct.id])
                // and the value is newCartItem
                return{
                   ...state,
                   items:{ ...state.items, [addedProduct.id]: updatedOrNewCartItem},
                    // totalAmount should be our old total amount plus prodprice
                    totalAmount: state.totalAmount+prodPrice
                };
            
        case REMOVE_FROM_CART: 
        // we have two cases if there is one item in the cart we want to delete it
        // if there is two items in a cart we want to reduce its number
        // we have to define what our quantity is
        // state.items is an object thus we can use action.pid
           const selectedCartItem=state.items[action.pid];
           const currentQuantity=selectedCartItem.quantity;
           
           let updatedCartItems;// as a avriable
           if(currentQuantity>1){
            //    in this case we are reducig quantity

            const updatedCartItem=new CartItem(
                selectedCartItem.quantity-1, 
                selectedCartItem.productPrice,
                selectedCartItem.productTitle, 
                selectedCartItem.sum-selectedCartItem.productPrice
                );
                // updated cart item needs to replace current cart item in our object
                
                updatedCartItems={...state.items, [action.pid]: updatedCartItem}//with state.item we passing old data, with updatedCartItem we replacing our old cart item with the updated quantity and sum
           }else{
            //here we bring back all items except from the item 
            // we want to reduce
            updatedCartItems={...state.items};
            delete updatedCartItems[action.pid];// will delete product item from copied javascript cart items object

           }
           return{
               ...state,
               items: updatedCartItems,
               totalAmount: state.totalAmount - selectedCartItem.productPrice
           }
           case ADD_ORDER:
               return initialState;// taip isvalom cart'a nes initial state nieko nera
           case DELETE_PRODUCT:
               if (!state.items[action.pid]){//checking if the item exists in a cart
                   return state;//jei positive tada jis compiliuoja apacioj esanti koda jei ne tei tsg grazina state kadangi nieks nepasikeite
               }
               const updatedItems={...state.items};//here we copying existing state items
               const itemTotal=state.items[action.pid].sum;//getting total sum of items 
               delete updatedItems[action.pid];//deleting item from the cart
               return {
                   ...state,
                   items: updatedItems,//items is the updatedproducts without our deleted item
                   totalAmount: state.totalAmount - itemTotal//we deleting price of deleted item from our cart
               }

    }
    // it return a ne state as our data
    return state;

};


// import { ADD_TO_CART } from '../actions/cart';
// import CartItem from '../../models/cart-item';

// const initialState = {
//   items: {},
//   totalAmount: 0
// };

// export default (state = initialState, action) => {
//   switch (action.type) {
//     case ADD_TO_CART:
//       const addedProduct = action.product;
//       const prodPrice = addedProduct.price;
//       const prodTitle = addedProduct.title;

//       let updatedOrNewCartItem;

//       if (state.items[addedProduct.id]) {
//         // already have the item in the cart
//         updatedOrNewCartItem = new CartItem(
//           state.items[addedProduct.id].quantity + 1,
//           prodPrice,
//           prodTitle,
//           state.items[addedProduct.id].sum + prodPrice
//         );
//       } else {
//         updatedOrNewCartItem = new CartItem(1, prodPrice, prodTitle, prodPrice);
//       }
//       return {
//         ...state,
//         items: { ...state.items, [addedProduct.id]: updatedOrNewCartItem },
//         totalAmount: state.totalAmount + prodPrice
//       };
//   }
//   return state;
// };