import PRODUCTS from '../../data/dummy-data.js'
import {DELETE_PRODUCT, CREATE_PRODUCT, UPDATE_PRODUCT} from '../actions/products'
import Product from '../../models/product.js';

const initialState={
    // all products or products you havent created
    availableProducts:PRODUCTS,
    // userCreaed products
    userProducts:PRODUCTS.filter(prod=>prod.ownerId==='u1')
};
export default (state=initialState, action)=>{
    switch(action.type){
        case CREATE_PRODUCT:
            const newProduct=new Product(
            new Date().toString(), //dummy id
            'u1', 
            action.productData.title, //retrieving data from action
            action.productData.imageUrl, 
            action.productData.description,
            action.productData.price);
            return{
                ...state,
                availableProducts: state.availableProducts.concat(newProduct),//contact yra old array plus new element in our case newProduct
                userProducts: state.userProducts.concat(newProduct)
            }
        case UPDATE_PRODUCT:
            const productIndex=state.userProducts.findIndex(prod=>prod.id===action.pid)//we are finding the index of the current product we want to update
            const updatedProduct=new Product(
                action.pid,
                state.userProducts[productIndex].ownerId,//we need to have the same id of owner thus we dont change anything here
                action.productData.title, //however we store new title for example
                action.productData.imageUrl, //however we store new imageUrl for example
                action.productData.description,//however we store new description for example
                state.userProducts[productIndex].price//price doesnt change
                
            )
            const updatedUserProducts=[...state.userProducts];//here (updatedUserProducts) we store copy od existing user products
            updatedUserProducts[productIndex]=updatedProduct;//from that array of updatedUserProducts we find the product and set it to be equal to our newly udated product(updatedProduct)
            const availableProductIndex= state.availableProducts.findIndex(//we are finding the index of the current product we want to update from availableProducts array because the id is different from useproducts
                prod=>prod.id===action.pid
            )
            const updatedAvailableProducts=[...state.availableProducts];
            updatedAvailableProducts[availableProductIndex]=updatedProduct;//we replace existing product with the updated product in availableproducts array
            return {
                ...state,
                availableProducts: updatedAvailableProducts,
                userProducts: updatedUserProducts
            }
        case DELETE_PRODUCT:
            return {//products needs to be deleted from userProducts array and from availableProducts array (from all app)
                ...state,
                userProducts: state.userProducts.filter(product=>//filter() return new array which is ran on function and if the function return true wee keep the item id it return false we dont keep the item
                    product.id!==action.pid// it will keep the products if the id doesnt match but if ittmacthes we then delete the product
                    ),
                availableProducts: state.availableProducts.filter(product=>
                    product.id!==action.pid
                    ),
                
            };
    }

    return state;
}
