import { ADD_ORDER } from "../actions/orders";
import Order from "../../models/order";

const initialState={
    orders:[]
}

export default (state= initialState, action)=>{
    switch(action.type){
        case ADD_ORDER:
            // new order stored in a new order constant with the order model we created
            const newOrder=new Order(new Date().toString(),//as id 
            action.orderData.items, 
            action.orderData.amount, 
            new Date()//current time stamp
            );
            return {
                ...state,//copying the old state, which is useless because we dont
                         // have any other state, but in case we have more complex state in the future it will be usefull
                orders: state.orders.concat(newOrder)//cancat adds new item into array and returns new array that includes that item
                         // old array stays untouched new array gets returned
            };

    }

    return state;
    
};