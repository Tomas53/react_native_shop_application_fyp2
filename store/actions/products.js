export const DELETE_PRODUCT ='DELETE_PRODUCT';//identifier DELETE_PRODUCT
export const CREATE_PRODUCT ='CREATE_PRODUCT';
export const UPDATE_PRODUCT ='UPDATE_PRODUCT';


export const deleteProduct= productId=>{//we are exporting action creator deleteProduct
    return { type: DELETE_PRODUCT, pid: productId };//actioncreator return action object, pid is product id thus we use forwarded productId
}

export const createProduct=(title, description, imageUrl, price)=>{//createProdut is action creator
    return{
        type: CREATE_PRODUCT,
        
        productData:{
            title: title,//title mapped to title above
            description: description,//description mapped to description above and so on
            imageUrl: imageUrl,
            price: price
        }

    }
}

export const updateProduct=(id, title, description, imageUrl)=>{//here we have id to identify wich product we are updating, we dont have price because we chose to make price not udatable
    return{
        type: UPDATE_PRODUCT,
        pid: id,
        productData:{
            title: title,
            description: description,
            imageUrl: imageUrl,
           
        }

    }
}