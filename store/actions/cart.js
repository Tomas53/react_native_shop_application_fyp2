// export a constant action identifier
export const ADD_TO_CART='ADD_TO_CART';
export const REMOVE_FROM_CART='REMOVE_FROM_CART';

// action creator
// we have full product object that will be used to pull
// out all data we need
export const addToCart=product=>{
    // then we return sum action object
    // which has type and the our product
    return {type: ADD_TO_CART, product: product};
};
// action creator function
// we need a product id that needs to be removed
// thus we use 'productId' for that
export const removeFromCart = productId => {
    // it removes a new action object where tyoes is REMOVE_FROM_CART
    return {type: REMOVE_FROM_CART, pid: productId}
}
