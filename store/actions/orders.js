export const ADD_ORDER='ADD_ORDER';//identifier

export const addOrder= (cartItems, totalAmount)=>{//recieves cartitems and total amount
    return {
        type: ADD_ORDER, 
        orderData:{ items: cartItems, amount: totalAmount
        //order data key where we merge cart items and total amount          
        }
        }
}