import React from 'react';
import {HeaderButton  } from 'react-navigation-header-buttons';
import {Ionicons} from '@expo/vector-icons';
import {Platform} from 'react-native';

import Colors from '../../constants/Colours'

const CustomHeaderButton=props=>{
// receives all the props (...props)
return <HeaderButton {...props} IconComponent={Ionicons} iconSize={2} color={"black"}/>
};
export default HeaderButton;//jkjk