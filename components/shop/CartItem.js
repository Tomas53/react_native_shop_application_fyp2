import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from "react-native";
import {Ionicons} from "@expo/vector-icons"

const CartItem=props=>{
    return( <View style={styles.cartItem}>
        <Text style={styles.itemData}>
            <Text styles={styles.quantity}>{props.quantity}</Text>
            <Text styles={styles.mainText}> {props.title}</Text>
            
        </Text>
        <View style={styles.itemData}>
            <Text styles={styles.mainText}>${props.amount.toFixed(2)}</Text>
           {props.deletable&&// jei deletable is true then we output touchableopacity
            <TouchableOpacity onPress={props.onRemove} style={styles.deleteButton}>
                <Ionicons 
                  name="md-trash"
                  size={21}
                  color="red"//sdsd
                />
            </TouchableOpacity>}
        </View>

    </View>
    )
};

const styles=StyleSheet.create({
    cartItem:{
        padding: 10,
        backgroundColor: 'white',
        flexDirection: 'row',
        justifyContent: "space-between",
        marginHorizontal: 21
    },
    itemData: {
        flexDirection: "row",
        alignItems: "center"
    },
    quantity:{
        color: 'grey',
        fontSize: 15
        
    },
    mainText: {
        fontSize: 15
    },
    
    deleteButton: {
        marginLeft: 20
    }


});

export default CartItem;