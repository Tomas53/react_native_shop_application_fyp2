import React from 'react';
import {
    createStackNavigator,
    createDrawerNavigator,
    createAppContainer
  } from 'react-navigation';
// import { createDrawerNavigator} from '@react-navigation/drawer';
// for custumization on different plaforms
import {Platform} from 'react-native';

import ProductDetailScreen from '../screens/shop/ProductDetailsScreen'
import UserProductsScreen from "../screens/user/UserProductsScreen"
import ProductsOverviewScreen from '../screens/shop/ProductsOverviewScreen';
import EditProductScreen from '../screens/user/EditProductScreen'


import Colours from '../constants/Colours';
import CartScreen from '../screens/shop/CartScreen';
import OrdersScreen from '../screens/shop/OrderScreen';
import {Ionicons} from '@expo/vector-icons';


const defaultNavOptions={
    headerStyle:{
        backgroundColor:Platform.OS==='android'? Colours.primary:''
    },
    headerTintColor: Platform.OS==='android'? 'white':Colours.primary
}


const ProductsNavigator=createStackNavigator({
    // ProductsOverview cia vardas kuri duodam siatm key
    ProductsOverview: ProductsOverviewScreen,
    ProductDetail:ProductDetailScreen,
    Cart:CartScreen

},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-cart':'ios-cart'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
});

const OrdersNavigator= createStackNavigator({
    Orders: OrdersScreen
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-list':'ios-list'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})


const AdminNavigator= createStackNavigator({
    UserProducts: UserProductsScreen,
    EditProduct: EditProductScreen//editproduct is identifier
},{
    navigationOptions:{
        drawerIcon: drawerConfig=>(<Ionicons name={
            Platform.Os==='android'?'md-create':'ios-create'
        }
        size={22}
        color={drawerConfig.tintColor}//draweris leidzia pakeisti spalva automatiskai priklausant ar mygtukas paspaustas ar ne
        />
        )
    },
    defaultNavigationOptions: defaultNavOptions
})


const ShopNavigator= createDrawerNavigator({
    Products: ProductsNavigator,
    Orders: OrdersNavigator,
    Admin: AdminNavigator
},{
    contentOptions: {
        activeTintColor: Colours.primary
    }
})

export default createAppContainer(ShopNavigator)
